package main

import "strings"

type round struct {
	Them string
	Us   string
}

func main() {

}

func parseInput(input string) []round {
	var rounds []round = make([]round, 0)

	for _, line := range strings.Split(input, "\n") {
		plays := strings.Split(line, " ")
		rounds = append(rounds, round{Them: plays[0], Us: plays[1]})
	}

	return rounds
}
