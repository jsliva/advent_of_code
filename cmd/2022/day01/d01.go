package main

import (
	"advent_of_code/internal/utils"
	"fmt"
	"os"
	"sort"
	"strconv"
	"strings"
)

func main() {
	input, err := utils.ReadFile("resources/2022/d01.txt")
	if err != nil {
		fmt.Println(err)
		os.Exit(1)
	}

	data := parseInput(input)
	sort.Ints(data)

	fmt.Printf("Part 1: %d\n", part1(data))
	fmt.Printf("Part 2: %d\n", part2(data))

	os.Exit(0)
}

func part1(input []int) int {
	return input[len(input)-1]
}

func part2(input []int) int {
	length := len(input)
	return input[length-1] + input[length-2] + input[length-3]
}

func parseInput(input string) []int {
	var packs []int = make([]int, 0)
	current := 0
	for _, line := range strings.Split(input, "\n") {
		if len(line) == 0 {
			packs = append(packs, current)
			current = 0
			continue
		}
		tmp, _ := strconv.Atoi(line)
		current += tmp
	}

	return packs
}
