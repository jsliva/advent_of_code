package main

import (
	"advent_of_code/internal/utils"
	"fmt"
	"math"
	"os"
	"slices"
	"strconv"
	"strings"
)

type Game struct {
	Id      int
	Winners []int
	Player  []int
}

func main() {
	input, err := utils.ReadFile("resources/2023/d04.txt")
	if err != nil {
		fmt.Println(err)
		os.Exit(1)
	}

	data := ParseInput(input)

	fmt.Printf("Part 1: %d\n", Part1(data))
	fmt.Printf("Part 2: %d\n", Part2(data))
}

func Part1(input []Game) int {
	total := 0

	for _, game := range input {
		matches := 0
		for _, num := range game.Player {
			if slices.Contains(game.Winners, num) {
				matches++
			}
		}
		if matches > 0 {
			if matches == 1 {
				total += 1
			} else {
				total += int(math.Pow(2, float64(matches-1)))
			}
		}
	}

	return total
}

func Part2(input []Game) int {
	processedCards := 0

	var cardPlays = make(map[int]int)
	for i := 1; i <= len(input); i++ {
		cardPlays[i] = 1
	}

	for _, game := range input {
		matches := 0
		for _, num := range game.Player {
			if slices.Contains(game.Winners, num) {
				matches++
			}
		}
		if matches > 0 {
			for i := 1; i <= matches; i++ {
				cardPlays[game.Id+i] += cardPlays[game.Id]
			}
		}

		processedCards += cardPlays[game.Id]
	}

	return processedCards
}

func ParseInput(input string) []Game {
	var games = make([]Game, 0)

	for i, line := range strings.Split(input, "\n") {
		// Remove the card identifier since it is the same as the line number
		segments := strings.Split(line, ": ")
		// Separate the winning numbers from the player's hand
		hands := strings.Split(segments[1], " | ")
		var winners = make([]int, 0)
		var player = make([]int, 0)
		for _, num := range strings.Fields(hands[0]) {
			n, _ := strconv.Atoi(num)
			winners = append(winners, n)
		}
		for _, num := range strings.Fields(hands[1]) {
			n, _ := strconv.Atoi(num)
			player = append(player, n)
		}

		games = append(games, Game{Id: i + 1, Winners: winners, Player: player})
	}

	return games
}
