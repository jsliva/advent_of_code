package main

import (
	"testing"
)

func TestPart1(t *testing.T) {
	input := "0 3 6 9 12 15\n1 3 6 10 15 21\n10 13 16 21 30 45"
	data := ParseInput(input)
	want := 114
	received := Part1(data)
	if want != received {
		t.Fatalf("Wanted %d, received %d\n", want, received)
	}
}

func TestPart2(t *testing.T) {
	input := "0 3 6 9 12 15\n1 3 6 10 15 21\n10 13 16 21 30 45"
	data := ParseInput(input)
	want := 2
	received := Part2(data)
	if want != received {
		t.Fatalf("Wanted %d, received %d\n", want, received)
	}
}
