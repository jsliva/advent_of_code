package main

import (
	"advent_of_code/internal/utils"
	"fmt"
	"os"
	"slices"
	"strconv"
	"strings"
)

func main() {
	input, err := utils.ReadFile("resources/2023/d09.txt")
	if err != nil {
		fmt.Println(err)
		os.Exit(1)
	}

	data := ParseInput(input)
	fmt.Printf("Part 1: %d\n", Part1(data))
	fmt.Printf("Part 2: %d\n", Part2(data))
}

func Part1(input [][]int) int {
	sumNewReadings := 0

	for _, readings := range input {
		var work = make([][]int, 0)
		work = append(work, readings)
		for {
			idx := len(work) - 1
			allZero := true
			for _, reading := range work[idx] {
				if reading != 0 {
					allZero = false
					break
				}
			}
			if allZero {
				break
			}
			var nextLine = make([]int, 0)
			for i := 1; i < len(work[idx]); i++ {
				nextLine = append(nextLine, work[idx][i]-work[idx][i-1])
			}
			work = append(work, nextLine)
		}

		newReading := 0
		for idx := len(work) - 1; idx > 0; idx-- {
			lastReadingIdx := len(work[idx]) - 1
			delta := work[idx][lastReadingIdx]
			newReading = work[idx-1][lastReadingIdx] + delta

			work[idx-1] = append(work[idx-1], newReading)
		}

		sumNewReadings += newReading
	}

	return sumNewReadings
}

func Part2(input [][]int) int {
	sumNewReadings := 0

	for _, readings := range input {
		var work = make([][]int, 0)
		work = append(work, readings)
		slices.Reverse(work)
		for {
			idx := len(work) - 1
			allZero := true
			for _, reading := range work[idx] {
				if reading != 0 {
					allZero = false
					break
				}
			}
			if allZero {
				break
			}
			var nextLine = make([]int, 0)
			for i := 0; i < len(work[idx])-1; i++ {
				nextLine = append(nextLine, work[idx][i+1]-work[idx][i])
			}
			work = append(work, nextLine)
		}

		newReading := 0
		for idx := len(work) - 1; idx > 0; idx-- {
			delta := work[idx][0]
			newReading = work[idx-1][0] - delta
			slices.Reverse(work[idx-1])
			work[idx-1] = append(work[idx-1], newReading)
			slices.Reverse(work[idx-1])
		}

		sumNewReadings += newReading
	}

	return sumNewReadings
}

func ParseInput(input string) [][]int {
	var readings = make([][]int, 0)

	for _, line := range strings.Split(input, "\n") {
		var nums = make([]int, 0)
		for _, num := range strings.Fields(line) {
			tmp, _ := strconv.Atoi(num)
			nums = append(nums, tmp)
		}
		readings = append(readings, nums)
	}

	return readings
}
