package main

import (
	"advent_of_code/internal/utils"
	"fmt"
	"os"
	"strconv"
	"strings"
	"unicode"
)

type Number struct {
	Value    int
	Included bool
	StartX   int
	EndX     int
	Y        int
}

type Symbol struct {
	Value    string
	X        int
	Y        int
	Adjacent []Number
}

type Data struct {
	Numbers []Number
	Symbols []Symbol
}

func main() {
	input, err := utils.ReadFile("resources/2023/d03.txt")
	if err != nil {
		fmt.Println(err)
		os.Exit(1)
	}

	data := ParseInput(input)

	fmt.Printf("Part 1: %d\n", Part1(data))
	fmt.Printf("Part 2: %d\n", Part2(data))
}

func Part1(input Data) int {
	sum := 0

	for _, n := range input.Numbers {
		if n.Included {
			sum += n.Value
		}
	}

	return sum
}

func Part2(input Data) int {
	sum := 0

	for _, s := range input.Symbols {
		if s.Value == "*" {
			if len(s.Adjacent) == 2 {
				sum += s.Adjacent[0].Value * s.Adjacent[1].Value
			}
		}
	}

	return sum
}

func ParseInput(input string) Data {
	var numbers = make([]Number, 0)
	var symbols = make([]Symbol, 0)

	for y, line := range strings.Split(input, "\n") {
		startX := -1
		value := 0

		for x, c := range line {
			if c == '.' {
				if value > 0 {
					numbers = append(numbers, Number{Value: value, StartX: startX, EndX: x - 1, Y: y, Included: false})
					value = 0
					startX = -1
				}
				continue
			} else if unicode.IsDigit(c) {
				if value == 0 {
					value, _ = strconv.Atoi(string(c))
					startX = x
				} else {
					tmp, _ := strconv.Atoi(string(c))
					value = (value * 10) + tmp
				}
			} else {
				symbols = append(symbols, Symbol{Value: string(c), X: x, Y: y, Adjacent: make([]Number, 0)})
				if value > 0 {
					numbers = append(numbers, Number{Value: value, StartX: startX, EndX: x - 1, Y: y, Included: false})
					value = 0
					startX = -1
				}
			}
		}
		if value > 0 {
			numbers = append(numbers, Number{Value: value, StartX: startX, EndX: len(line), Y: y, Included: false})
		}
	}

numberIterator:
	for i, _ := range numbers {
		for j, _ := range symbols {
			if symbols[j].Y == numbers[i].Y && (symbols[j].X == numbers[i].StartX-1 || symbols[j].X == numbers[i].EndX+1) {
				numbers[i].Included = true
				if symbols[j].Value == "*" {
					symbols[j].Adjacent = append(symbols[j].Adjacent, numbers[i])
				}
				continue numberIterator
			}
			for x := numbers[i].StartX; x <= numbers[i].EndX; x++ {
				if symbols[j].Y == numbers[i].Y-1 || symbols[j].Y == numbers[i].Y+1 {
					if symbols[j].X == x || symbols[j].X == x-1 || symbols[j].X == x+1 {
						numbers[i].Included = true
						if symbols[j].Value == "*" {
							symbols[j].Adjacent = append(symbols[j].Adjacent, numbers[i])
						}
						continue numberIterator
					}
				}
			}
		}
	}

	return Data{Numbers: numbers, Symbols: symbols}
}
