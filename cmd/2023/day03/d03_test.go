package main

import "testing"

func TestPart1(t *testing.T) {
	input := "467..114..\n...*......\n..35..633.\n......#...\n617*......\n.....+.58.\n..592.....\n......755.\n...$.*....\n.664.598.."
	data := ParseInput(input)
	want := 4361
	received := Part1(data)
	if want != received {
		t.Fatalf("Wanted %d, received %d\n", want, received)
	}
}

func TestPart3(t *testing.T) {
	input := "467..114..\n...*......\n..35..633.\n......#...\n617*......\n.....+.58.\n..592.....\n......755.\n...$.*....\n.664.598.."
	data := ParseInput(input)
	want := 467835
	received := Part2(data)
	if want != received {
		t.Fatalf("Wanted %d, received %d\n", want, received)
	}
}
