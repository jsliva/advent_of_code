package main

import "testing"

func TestPart1(t *testing.T) {
	input := "1abc2\npqr3stu8vwx\na1b2c3d4e5f\ntreb7uchet"
	data := ParseInput(input)
	want := 142
	received := Part1(data)
	if want != received {
		t.Fatalf("Wanted %d, received %d\n", want, received)
	}
}

func TestPart2(t *testing.T) {
	input := "two1nine\neightwothree\nabcone2threexyz\nxtwone3four\n4nineeightseven2\nzoneight234\n7pqrstsixteen"
	data := ParseInput(input)
	want := 281
	received := Part2(data)
	if want != received {
		t.Fatalf("Wanted %d, received %d\n", want, received)
	}
}
