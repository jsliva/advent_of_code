package main

import (
	"advent_of_code/internal/utils"
	"fmt"
	"os"
	"strconv"
	"strings"
	"unicode"
)

func main() {
	input, err := utils.ReadFile("resources/2023/d01.txt")
	if err != nil {
		fmt.Println(err)
		os.Exit(1)
	}

	data := ParseInput(input)

	fmt.Printf("Part 1: %d\n", Part1(data))
	fmt.Printf("Part 2: %d\n", Part2(data))

}

func Part1(input []string) int {
	total := 0

	for _, line := range input {
		var digits []int = make([]int, 0)
		for _, c := range line {
			if unicode.IsDigit(c) {
				d, _ := strconv.Atoi(string(c))
				digits = append(digits, d)
			}
		}
		if len(digits) > 0 {
			total += (10 * digits[0]) + digits[len(digits)-1]
		}
	}

	return total
}

func Part2(input []string) int {
	words := []string{"one", "two", "three", "four", "five", "six", "seven", "eight", "nine"}
	digits := []string{"1", "2", "3", "4", "5", "6", "7", "8", "9"}
	var newLines []string = make([]string, len(input))
	for _, line := range input {
		pos := 0
		newLine := ""
		for {
			matched := false
			for i, _ := range words {
				if strings.HasPrefix(line[pos:], words[i]) {
					newLine += digits[i]
					matched = true
				}
			}
			if !matched {
				newLine += line[pos : pos+1]
			}
			pos++
			if pos == len(line) {
				break
			}
		}
		newLines = append(newLines, newLine)
	}

	return Part1(newLines)
}

func ParseInput(input string) []string {
	var lines []string = make([]string, 0)
	for _, line := range strings.Split(input, "\n") {
		lines = append(lines, line)
	}

	return lines
}
