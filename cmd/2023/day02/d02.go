package main

import (
	"advent_of_code/internal/utils"
	"fmt"
	"os"
	"strconv"
	"strings"
)

type Game struct {
	Id    int
	Hands []Hand
}

type Hand struct {
	Red   int
	Green int
	Blue  int
}

func main() {
	input, err := utils.ReadFile("resources/2023/d02.txt")
	if err != nil {
		fmt.Println(err)
		os.Exit(1)
	}

	data := ParseInput(input)

	fmt.Printf("Part 1: %d\n", Part1(data))
	fmt.Printf("Part 2: %d\n", Part2(data))
}

func Part1(input []Game) int {
	redLimit := 12
	greenLimit := 13
	blueLimit := 14
	validGameSum := 0

	for _, game := range input {
		valid := true
		for _, hand := range game.Hands {
			if hand.Red > redLimit || hand.Green > greenLimit || hand.Blue > blueLimit {
				valid = false
				break
			}
		}
		if valid {
			validGameSum += game.Id
		}
	}

	return validGameSum
}

func Part2(input []Game) int {
	totalPower := 0

	for _, game := range input {
		red := 0
		green := 0
		blue := 0

		for _, hand := range game.Hands {
			if hand.Red > red {
				red = hand.Red
			}
			if hand.Green > green {
				green = hand.Green
			}
			if hand.Blue > blue {
				blue = hand.Blue
			}
		}

		totalPower += red * green * blue
	}

	return totalPower
}

func ParseInput(input string) []Game {
	var games = make([]Game, 0)
	for _, game := range strings.Split(input, "\n") {
		segments := strings.Split(game, ": ")
		a := strings.Split(segments[0], " ")
		id, _ := strconv.Atoi(a[1])
		var hands = make([]Hand, 0)
		pulls := strings.Split(segments[1], "; ")
		for _, pull := range pulls {
			colors := strings.Split(pull, ", ")
			hand := Hand{Red: 0, Green: 0, Blue: 0}
			for _, color := range colors {
				b := strings.Split(color, " ")
				count, _ := strconv.Atoi(b[0])
				if b[1] == "red" {
					hand.Red += count
				} else if b[1] == "green" {
					hand.Green += count
				} else {
					hand.Blue += count
				}
			}
			hands = append(hands, hand)
		}
		games = append(games, Game{Id: id, Hands: hands})
	}

	return games
}
