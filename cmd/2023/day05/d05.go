package main

import (
	"advent_of_code/internal/utils"
	"fmt"
	"os"
	"strconv"
	"strings"
)

type Almanac struct {
	Seeds                 []int
	SeedToSoil            []SourceMap
	SoilToFertilizer      []SourceMap
	FertilizerToWater     []SourceMap
	WaterToLight          []SourceMap
	LightToTemperature    []SourceMap
	TemperatureToHumidity []SourceMap
	HumidityToLocation    []SourceMap
}

type SourceMap struct {
	SourceStart      int
	DestinationStart int
	Length           int
}

type SeedRange struct {
	Start int
	End   int
}

func getDestination(source int, destinations *[]SourceMap) int {
	for _, dest := range *destinations {
		if source >= dest.SourceStart && source <= dest.SourceStart+dest.Length {
			return dest.DestinationStart + (source - dest.SourceStart)
		}
	}

	return -1
}

func (a *Almanac) getFinalLocation(seed int) int {
	soilLocation := getDestination(seed, &a.SeedToSoil)
	if soilLocation == -1 {
		soilLocation = seed
	}

	fertilizerLocation := getDestination(soilLocation, &a.SoilToFertilizer)
	if fertilizerLocation == -1 {
		fertilizerLocation = soilLocation
	}

	waterLocation := getDestination(fertilizerLocation, &a.FertilizerToWater)
	if waterLocation == -1 {
		waterLocation = fertilizerLocation
	}

	lightLocation := getDestination(waterLocation, &a.WaterToLight)
	if lightLocation == -1 {
		lightLocation = waterLocation
	}

	temperatureLocation := getDestination(lightLocation, &a.LightToTemperature)
	if temperatureLocation == -1 {
		temperatureLocation = lightLocation
	}

	humidityLocation := getDestination(temperatureLocation, &a.TemperatureToHumidity)
	if humidityLocation == -1 {
		humidityLocation = temperatureLocation
	}

	location := getDestination(humidityLocation, &a.HumidityToLocation)
	if location == -1 {
		location = humidityLocation
	}

	return location
}

func main() {
	input, err := utils.ReadFile("resources/2023/d05.txt")
	if err != nil {
		fmt.Println(err)
		os.Exit(1)
	}

	data := ParseInput(input)

	fmt.Printf("Part 1: %d\n", Part1(data))
	fmt.Printf("Part 2: %d\n", Part2(data))
}

func Part1(input Almanac) int {
	minLocation := -1

	for _, seed := range input.Seeds {
		location := input.getFinalLocation(seed)

		if minLocation == -1 || location < minLocation {
			minLocation = location
		}
	}

	return minLocation
}

func Part2(input Almanac) int {
	minLocation := -1
	var seeds = make([]SeedRange, 0)
	for i := 0; i < len(input.Seeds)-1; i += 2 {
		seeds = append(seeds, SeedRange{Start: input.Seeds[i], End: input.Seeds[i] + input.Seeds[i+1] - 1})
	}

	for _, seed := range seeds {
		for i := seed.Start; i <= seed.End; i++ {
			location := input.getFinalLocation(i)
			if minLocation == -1 || location < minLocation {
				minLocation = location
			}
		}
	}

	return minLocation
}

func ParseInput(input string) Almanac {
	var seeds = make([]int, 0)
	var seedToSoil = make([]SourceMap, 0)
	var soilToFertilizer = make([]SourceMap, 0)
	var fertilizerToWater = make([]SourceMap, 0)
	var waterToLight = make([]SourceMap, 0)
	var lightToTemperature = make([]SourceMap, 0)
	var temperatureToHumidity = make([]SourceMap, 0)
	var humidityToLocation = make([]SourceMap, 0)

	for _, block := range strings.Split(input, "\n\n") {
		lines := strings.Split(block, "\n")
		parts := strings.Split(lines[0], ":")
		if parts[0] == "seeds" {
			for _, num := range strings.Fields(parts[1]) {
				tmp, _ := strconv.Atoi(num)
				seeds = append(seeds, tmp)
			}
		} else {
			for i := 1; i < len(lines); i++ {
				nums := strings.Fields(lines[i])
				destination, _ := strconv.Atoi(nums[0])
				source, _ := strconv.Atoi(nums[1])
				length, _ := strconv.Atoi(nums[2])
				tmp := SourceMap{DestinationStart: destination, SourceStart: source, Length: length}

				if parts[0] == "seed-to-soil map" {
					seedToSoil = append(seedToSoil, tmp)
				} else if parts[0] == "soil-to-fertilizer map" {
					soilToFertilizer = append(soilToFertilizer, tmp)
				} else if parts[0] == "fertilizer-to-water map" {
					fertilizerToWater = append(fertilizerToWater, tmp)
				} else if parts[0] == "water-to-light map" {
					waterToLight = append(waterToLight, tmp)
				} else if parts[0] == "light-to-temperature map" {
					lightToTemperature = append(lightToTemperature, tmp)
				} else if parts[0] == "temperature-to-humidity map" {
					temperatureToHumidity = append(temperatureToHumidity, tmp)
				} else {
					humidityToLocation = append(humidityToLocation, tmp)
				}
			}
		}
	}

	return Almanac{
		Seeds:                 seeds,
		SeedToSoil:            seedToSoil,
		SoilToFertilizer:      soilToFertilizer,
		FertilizerToWater:     fertilizerToWater,
		WaterToLight:          waterToLight,
		LightToTemperature:    lightToTemperature,
		TemperatureToHumidity: temperatureToHumidity,
		HumidityToLocation:    humidityToLocation,
	}
}
