package main

import (
	"advent_of_code/internal/utils"
	"fmt"
	"os"
	"sort"
	"strconv"
	"strings"
)

type Hand struct {
	Cards  []string
	Bid    int
	Type   HandType
	JCount int
}

type HandType int

const (
	HighCard HandType = iota
	OnePair
	TwoPair
	ThreeOfAKind
	FullHouse
	FourOfAKind
	FiveOfAKind
)

func main() {
	input, err := utils.ReadFile("resources/2023/d07.txt")
	if err != nil {
		fmt.Println(err)
		os.Exit(1)
	}

	data := ParseInput(input)
	fmt.Printf("Part 1: %d\n", Part1(data))
	fmt.Printf("Part 2: %d\n", Part2(data))
}

func Part1(input []Hand) int {
	totalWinnings := 0

	cardValues := map[string]int{
		"2": 2,
		"3": 3,
		"4": 4,
		"5": 5,
		"6": 6,
		"7": 7,
		"8": 8,
		"9": 9,
		"T": 10,
		"J": 11,
		"Q": 12,
		"K": 13,
		"A": 14,
	}

	sort.Slice(input, func(a, b int) bool {
		if input[a].Type == input[b].Type {
			for i := 0; i < 5; i++ {
				if cardValues[input[a].Cards[i]] == cardValues[input[b].Cards[i]] {
					continue
				}
				return cardValues[input[a].Cards[i]] < cardValues[input[b].Cards[i]]
			}
		}

		return input[a].Type < input[b].Type
	})

	for i, hand := range input {
		totalWinnings += hand.Bid * (i + 1)
	}

	return totalWinnings
}

func Part2(input []Hand) int {
	totalWinnings := 0

	cardValues := map[string]int{
		"J": 1,
		"2": 2,
		"3": 3,
		"4": 4,
		"5": 5,
		"6": 6,
		"7": 7,
		"8": 8,
		"9": 9,
		"T": 10,
		"Q": 11,
		"K": 12,
		"A": 13,
	}

	for i := range input {
		if input[i].JCount == 0 {
			continue
		}

		if input[i].JCount == 1 {
			if input[i].Type == HighCard {
				input[i].Type = OnePair
			} else if input[i].Type == OnePair {
				input[i].Type = ThreeOfAKind
			} else if input[i].Type == TwoPair {
				input[i].Type = FullHouse
			} else if input[i].Type == ThreeOfAKind {
				input[i].Type = FourOfAKind
			} else if input[i].Type == FourOfAKind {
				input[i].Type = FiveOfAKind
			}
		} else if input[i].JCount == 2 {
			if input[i].Type == OnePair {
				input[i].Type = ThreeOfAKind
			} else if input[i].Type == TwoPair {
				input[i].Type = FourOfAKind
			} else if input[i].Type == FullHouse {
				input[i].Type = FiveOfAKind
			}
		} else if input[i].JCount == 3 {
			if input[i].Type == ThreeOfAKind {
				input[i].Type = FourOfAKind
			} else if input[i].Type == FullHouse {
				input[i].Type = FiveOfAKind
			}
		} else if input[i].JCount == 4 {
			if input[i].Type == FourOfAKind {
				input[i].Type = FiveOfAKind
			}
		}
	}

	sort.Slice(input, func(a, b int) bool {
		if input[a].Type == input[b].Type {
			for i := 0; i < 5; i++ {
				if cardValues[input[a].Cards[i]] == cardValues[input[b].Cards[i]] {
					continue
				}
				return cardValues[input[a].Cards[i]] < cardValues[input[b].Cards[i]]
			}
		}

		return input[a].Type < input[b].Type
	})

	for i, hand := range input {
		totalWinnings += hand.Bid * (i + 1)
	}

	return totalWinnings
}

func ParseInput(input string) []Hand {
	var hands = make([]Hand, 0)

	for _, line := range strings.Split(input, "\n") {
		parts := strings.Fields(line)
		bid, _ := strconv.Atoi(parts[1])
		var cards = strings.Split(parts[0], "")
		hand := Hand{Cards: cards, Bid: bid, JCount: 0}
		var tmp = make(map[string]int)
		for _, c := range cards {
			tmp[c]++
		}

		if count, ok := tmp["J"]; ok {
			hand.JCount = count
		}

		if len(tmp) == 1 {
			hand.Type = FiveOfAKind
		} else if len(tmp) == 5 {
			hand.Type = HighCard
		} else if len(tmp) == 4 {
			hand.Type = OnePair
		} else if len(tmp) == 2 {
			maxCount := 0
			for _, val := range tmp {
				if val > maxCount {
					maxCount = val
				}
			}
			if maxCount == 4 {
				hand.Type = FourOfAKind
			} else {
				hand.Type = FullHouse
			}
		} else {
			maxCount := 0
			for _, val := range tmp {
				if val > maxCount {
					maxCount = val
				}
			}
			if maxCount == 3 {
				hand.Type = ThreeOfAKind
			} else {
				hand.Type = TwoPair
			}
		}

		hands = append(hands, hand)
	}

	return hands
}
