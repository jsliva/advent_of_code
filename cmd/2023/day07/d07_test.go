package main

import (
	"testing"
)

func TestPart1(t *testing.T) {
	input := "32T3K 765\nT55J5 684\nKK677 28\nKTJJT 220\nQQQJA 483"
	data := ParseInput(input)
	want := 6440
	received := Part1(data)
	if want != received {
		t.Fatalf("Wanted %d, received %d\n", want, received)
	}
}

func TestPart2(t *testing.T) {
	input := "32T3K 765\nT55J5 684\nKK677 28\nKTJJT 220\nQQQJA 483"
	data := ParseInput(input)
	want := 5905
	received := Part2(data)
	if want != received {
		t.Fatalf("Wanted %d, received %d\n", want, received)
	}
}
