package main

import (
	"advent_of_code/internal/utils"
	"fmt"
	"os"
	"regexp"
	"strings"
)

type Node struct {
	Name  string
	Left  string
	Right string
}

type Map struct {
	Orders []string
	Nodes  map[string]Node
}

func main() {
	input, err := utils.ReadFile("resources/2023/d08.txt")
	if err != nil {
		fmt.Println(err)
		os.Exit(1)
	}

	data := ParseInput(input)
	fmt.Printf("Part 1: %d\n", Part1(data))
	fmt.Printf("Part 2: %d\n", Part2(data))
}

func Part1(input Map) int {
	steps := 0
	currentRoom := "AAA"
	i := 0

	for {
		if currentRoom == "ZZZ" {
			break
		}

		if input.Orders[i] == "L" {
			currentRoom = input.Nodes[currentRoom].Left
		} else {
			currentRoom = input.Nodes[currentRoom].Right
		}

		i++
		if i == len(input.Orders) {
			i = 0
		}
		steps++
	}

	return steps
}

func Part2(input Map) int {
	var startingNodes = make([]string, 0)
	var iterations = make([]int, 0)

	for name := range input.Nodes {
		if name[2:] == "A" {
			startingNodes = append(startingNodes, name)
		}
	}

	for _, start := range startingNodes {
		i := 0
		steps := 0
		current := start

		for {
			if current[2:] == "Z" {
				iterations = append(iterations, steps)
				break
			}

			if input.Orders[i] == "L" {
				current = input.Nodes[current].Left
			} else {
				current = input.Nodes[current].Right
			}

			i++
			if i == len(input.Orders) {
				i = 0
			}
			steps++
		}
	}

	return lcm(iterations[0], iterations[1], iterations[2], iterations[3], iterations[4], iterations[5])
}

func ParseInput(input string) Map {
	var nodes = make(map[string]Node)

	segments := strings.Split(input, "\n\n")
	orders := strings.Split(segments[0], "")

	nameRegex := regexp.MustCompile("[0-9A-Z]{3}")

	for _, line := range strings.Split(segments[1], "\n") {
		rooms := nameRegex.FindAllString(line, -1)
		tmp := Node{Name: rooms[0], Left: rooms[1], Right: rooms[2]}
		nodes[rooms[0]] = tmp
	}

	return Map{Orders: orders, Nodes: nodes}
}

func gcd(a, b int) int {
	for b != 0 {
		t := b
		b = a % b
		a = t
	}
	return a
}

func lcm(a, b int, nums ...int) int {
	result := a * b / gcd(a, b)

	for i := 0; i < len(nums); i++ {
		result = lcm(result, nums[i])
	}

	return result
}
