package main

import (
	"testing"
)

func TestPart1(t *testing.T) {
	input := "LLR\n\nAAA = (BBB, BBB)\nBBB = (AAA, ZZZ)\nZZZ = (ZZZ, ZZZ)"
	data := ParseInput(input)
	want := 6
	received := Part1(data)
	if want != received {
		t.Fatalf("Wanted %d, received %d\n", want, received)
	}
}

func TestPart2(t *testing.T) {
	input := "LR\n\n11A = (11B, XXX)\n11B = (XXX, 11Z)\n11Z = (11B, XXX)\n22A = (22B, XXX)\n22B = (22C, 22C)\n22C = (22Z, 22Z)\n22Z = (22B, 22B)\nXXX = (XXX, XXX)"
	data := ParseInput(input)
	want := 6
	received := Part2(data)
	if want != received {
		t.Fatalf("Wanted %d, received %d\n", want, received)
	}
}
