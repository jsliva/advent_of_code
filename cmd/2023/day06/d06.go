package main

import (
	"advent_of_code/internal/utils"
	"fmt"
	"os"
	"strconv"
	"strings"
)

type Race struct {
	Time     int
	Distance int
}

func main() {
	input, err := utils.ReadFile("resources/2023/d06.txt")
	if err != nil {
		fmt.Println(err)
		os.Exit(1)
	}

	data := ParseInput(input)
	fmt.Printf("Part 1: %d\n", Part1(data))

	data2 := ParseInput2(input)
	fmt.Printf("Part 2: %d\n", Part2(data2))
}

func Part1(input []Race) int {
	marginOfError := 1

	for _, race := range input {
		wins := 0
		for t := 0; t <= race.Time; t++ {
			distance := t * (race.Time - t)
			if distance > race.Distance {
				wins++
			}
		}
		marginOfError *= wins
	}

	return marginOfError
}

func Part2(input Race) int {
	marginOfError := 1

	wins := 0
	for t := 0; t <= input.Time; t++ {
		distance := t * (input.Time - t)
		if distance > input.Distance {
			wins++
		}
	}
	marginOfError *= wins

	return marginOfError
}

func ParseInput(input string) []Race {
	var races = make([]Race, 0)
	var times = make([]int, 0)
	var distances = make([]int, 0)

	for _, line := range strings.Split(input, "\n") {
		segments := strings.Split(line, ":")
		tmp := strings.Fields(segments[1])
		for i := range tmp {
			num, _ := strconv.Atoi(tmp[i])
			if segments[0] == "Time" {
				times = append(times, num)
			} else {
				distances = append(distances, num)
			}
		}
	}

	for i := range times {
		races = append(races, Race{Time: times[i], Distance: distances[i]})
	}

	return races
}

func ParseInput2(input string) Race {
	race := Race{}

	for _, line := range strings.Split(input, "\n") {
		tmp := strings.Replace(line, " ", "", -1)
		segments := strings.Split(tmp, ":")
		num, _ := strconv.Atoi(segments[1])
		if segments[0] == "Time" {
			race.Time = num
		} else {
			race.Distance = num
		}
	}

	return race
}
