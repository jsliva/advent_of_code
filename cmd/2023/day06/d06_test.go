package main

import (
	"testing"
)

func TestPart1(t *testing.T) {
	input := "Time:      7  15   30\nDistance:  9  40  200"
	data := ParseInput(input)
	want := 288
	received := Part1(data)
	if want != received {
		t.Fatalf("Wanted %d, received %d\n", want, received)
	}
}

func TestPart2(t *testing.T) {
	input := "Time:      7  15   30\nDistance:  9  40  200"
	data := ParseInput2(input)
	want := 71503
	received := Part2(data)
	if want != received {
		t.Fatalf("Wanted %d, received %d\n", want, received)
	}
}
